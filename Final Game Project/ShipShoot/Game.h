#pragma once

#include <vector>


#include "Input.h"
#include "D3D.h"
#include "SpriteBatch.h"
#include "Sprite.h"
#include "SpriteFont.h"
#include <Audio.h>


namespace GC
{
	const float SCROLL_SPEED = 10.f;
	const int BGND_LAYERS = 3;
	const float MOUSE_SPEED = 5000;
	const float PAD_SPEED = 500;
	const float ASTEROID_SPEED = 100;
	const float ASTEROID_SPAWN_RATE = 1;
	const float ASTEROID_SPAWN_INC = 1;
	const float ASTEROID_MAX_SPAWN_DELAY = 10;
	const int ROID_CACHE = 32;
}

/*
Animated missile bullet 
Player can only fire one and has to wait for it to leave the 
screen before firing again.
*/
struct Bullet
{
	Bullet(MyD3D& d3d)
		:bullet(d3d)
	{}
	Sprite bullet;
	bool active = false;

	void Init(MyD3D& d3d);
	void Render(DirectX::SpriteBatch& batch);
	void Update(float dTime);
	const float MISSILE_SPEED = 500;
};

/*
Animated asteroid
*/
struct Asteroid
{
	Asteroid(MyD3D& d3d)
		:spr(d3d)
	{}
	Sprite spr;
	bool active = false;	//should it render and animate?

	//setup
	void Init();
	/*
	draw - the atlas has two asteroids, half frames in one, half the other
	asteroids are randomly assigned one and then animate and at random fps
	*/
	void Render(DirectX::SpriteBatch& batch);
	/*
	move left until offscreen, then go inactive
	*/
	void Update(float dTime);
};

//--------Score class
class Score
{
public:
	void Init();
	int getAmount() const;
	void updateAmount(int);
	void updateBossAmount(int);
private:
	int amount_;
};
//--------end of Score class


class IntroMode
{
public:
	IntroMode(MyD3D& d3d);
	void Render(float dTime, DirectX::SpriteBatch& batch, DirectX::SpriteFont& font);

	void Init();
private:
	static const int BGND_LAYERS = 4;

	MyD3D& mD3D;
	std::vector<Sprite> mBgnd; //parallax layers

};

//horizontal scrolling with player controlled ship
class PlayMode
{
public:
	PlayMode(MyD3D& d3d);
	void Update(float dTime);
	void Render(float dTime, DirectX::SpriteBatch& batch, int& s);
	int health = 5;
	int bossHealth = 50;
	float bossRotationVelocity = 0.01f;
	float bossVelocity = 55.f;
	bool bossDead = false;
	float bossSpawnMove = 1;
	float timer;
	bool TimerSet = false;
	Score score;
	float scaleMod = 0;



private:
	const float SCROLL_SPEED = 20.f;
	static const int BGND_LAYERS = 3;
	const float SPEED = 250;
	const float MOUSE_SPEED = 5000;
	const float PAD_SPEED = 500;
	

	MyD3D& mD3D;
	std::vector<Sprite> mBgnd; //parallax layers
	Sprite mPlayer;		//jet
	RECTF mPlayArea;	//don't go outside this	
	Sprite mThrust;		//flames out the back
	Bullet mMissile;	//weapon, only one at once
	std::vector<Asteroid> mAsteroids;	//a cache of asteroids
	float mSpawnRateSec = GC::ASTEROID_SPAWN_RATE; //how fast to spawn in new asteroids
	float mLastSpawn = 0;	//used in spawn timing
	Sprite mCoinSpin;		// sprite for the spinning coin
	DirectX::SpriteFont* mpFont = nullptr; // sprite for font
	Sprite mHealth;			// sprite for displaying playing health bar
	Sprite mBossChar;		// sprite for displaying the boss
	Sprite mBossHealth;		// sprite for displaying the boss health bar


	//once we start thrusting we have to keep doing it for 
	//at least a fraction of a second or it looks whack
	float mThrusting = 0; 
	//setup once
	void InitBgnd();
	void InitPlayer();
	void InitScore();
	void InitHealth();
	void InitBossChar();
	void InitBossHealth();
	//make it move, reset it once it leaves the screen, only one at once
	void UpdateMissile(float dTime);
	//make it scroll parallax
	void UpdateBgnd(float dTime);
	//move the ship by keyboard, gamepad or mouse
	void UpdateInput(float dTime);
	//make the flames wobble when the ship moves
	void UpdateThrust(float dTime);
	// makes the players health decrease if they have been hit
	void UpdateHealth(float dTime);
	// makes the boss spawn after the player gets a set score
	void UpdateBossChar(float dTime);
	// makes the bosses health bar decrease
	void UpdateBossHealth(float dTime);
	
	//scroll in asteroids randomly
	/*
	Try to make one just off screen to the right that definitely
	isn't touching anything else.
	*/
	Asteroid* SpawnRoid();
	/*
	Give each active roid an update and monitor the spawn delay
	creating new roids as needed
	*/
	void UpdateRoids(float dTime);
	//create a cache of roids to use and re-use
	void InitRoids();
	//ask each roid if it wants to render
	void RenderRoids(DirectX::SpriteBatch& batch);
	//me - an asteroid to check for collision with all other active ones
	Asteroid* CheckCollRoids(Asteroid& me);

};


class GameOverMode
{
public:
	GameOverMode(MyD3D& d3d);
	void Render(float dTime, DirectX::SpriteBatch& batch, DirectX::SpriteFont& font, int& s);
	void UpdateText(DirectX::SpriteBatch& batch, DirectX::SpriteFont& font, int& s);
	void Init();
private:
	static const int BGND_LAYERS = 3;

	MyD3D& mD3D;
	std::vector<Sprite> mBgnd; //parallax layers

};

/*
Basic wrapper for a game
*/
class Game
{
public:
	enum class State { START, PLAY, GAMEOVER };
	static MouseAndKeys sMKIn;
	static Gamepads sGamepads;
	State state = State::START;
	Game(MyD3D& d3d);


	void Release();
	void Update(float dTime);
	void Render(float dTime);
private:
	MyD3D& mD3D;
	DirectX::SpriteBatch* mpSB = nullptr;
	DirectX::SpriteFont* mpF = nullptr;
	// game modes
	IntroMode mIMode;
	PlayMode mPMode;
	GameOverMode mGOMode;

};


